import json
import requests
from .keys import PEXELS_API_KEY, OPENWEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_lat_long(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    params = {
        "q": city + "," + state + "," + "840",
        "appid": OPENWEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {"lat": content[0]["lat"], "lon": content[0]["lon"]}
    except (KeyError, IndexError):
        return {"lat": None, "lon": None}


def get_weather(latlon):
    url = "https://api.openweathermap.org/data/2.5/weather?"
    params = {
        "lat": latlon["lat"],
        "lon": latlon["lon"],
        "appid": OPENWEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "temperature": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return {"weather": None}
