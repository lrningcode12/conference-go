import json
import pika
import django
import os
import time
import sys
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    email = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{email['presenter_name']}/n, we're happy to tell you that your presentation {email['title']} has been accepted",
        "admin@conference.go",
        [{email["presenter_email"]}],
        fail_silently=False,
    )


# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue="presentation_approvals")
# channel.basic_consume(
#     queue="presentation_approvals",
#     on_message_callback=process_approval,
#     auto_ack=True,
# )
# channel.start_consuming()


def process_rejection(ch, method, properties, body):
    email = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{email['presenter_name']}/n, we're happy to tell you that your presentation {email['title']} has been accepted",
        "admin@conference.go",
        [{email["presenter_email"]}],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_rejections")
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
